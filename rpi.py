import RPi.GPIO as GPIO
import requests
import os


GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(25, GPIO.OUT, initial=GPIO.LOW)

GPIO.add_event_detect(4, GPIO.RISING)
import datetime
global stTime,ct
stTime = datetime.datetime.now()
ct = 1
global working
working = False
#GPIO.cleanup()
def my_callback(arg):
	print arg
	print "Callback recieved"
	GPIO.output(25, GPIO.input(4))
	global stTime,ct, working
	curTime = datetime.datetime.now()
	checkTime = (curTime-stTime).total_seconds()
	
	print int(checkTime)
	if not working and checkTime>10:
		working = True
		
		os.system("/opt/picoscope/bin/ps2000Con")
		ct += 1
		stTime = curTime
		d=''
		with open("fast_stream.txt" ) as strm:
			for line in strm.readlines():
				d += line.split(',')[1]+'\n';
		#print d
		working = False 
		#requests.post("http://wamp.cioc.in:8090/notify", json = {"topic" : "service.chat.admin" , "args" : [{"data" : d }]})
		requests.post("http://10.42.0.1:8080/notify", json = {"topic" : "service.chat.admin" , "args" : [{"data" : d }]})
		print "Data Sent"
		
		
GPIO.add_event_callback(4, my_callback)

print "Program ended"

input('running')
GPIO.cleanup()
